package schule;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/*
 * Hauptklasse Enthält neben der graphische Benutzeroberfläche (GUI: Graphical
 * User Interface) durch Erbung von der Klasse JFrame, auch die Spielmechanik.
 * Die Spielmechanik besteht aus dem Wechseln des Spielers, dem Überprüfen des
 * Zustandes des Spieles und der Behandlung von Ereignisssen wie von dem
 * Gewinnen eines Spielers.
 * 
 * Unterklasse von JFrame
 */

@SuppressWarnings("serial")
public class Spiel extends JFrame {

	/*
	 * Statische Instanz der Klasse Spiel, auf die in der Klasse SpielButton
	 * über Spiel.spiel zugegriffen werden kann. Dies wird verwendet, um kein
	 * neues Objekt der Klasse Spiel in SpielButton erstellen zu müssen. Es wird
	 * der Zugriff auf die Methoden der Klasse Spiel also vereinfacht.
	 */

	public static Spiel spiel;

	/*
	 * Spieler1 = O | Spieler2 = X | Gewonnen = G
	 */

	private char spieler;

	/*
	 * Speichert die erstellten Buttons in einem Array. Das Array hat 9 Plätze
	 */

	private SpielButton[] buttons;

	/*
	 * Konstruktor der Klasse Spiel Ruft den Konstruktor der Überklasse auf. Der
	 * Übergabewert ist der Titel des Fensters. Die Methode setSize bestimmt die
	 * Größei des erstellten Fensters. Die Methode setVisible macht das
	 * erstellte Fenster sichtbar. Die Methode setDefaultCloseOperation setzt
	 * die Standardeinstellung für das Schließen des Fensters. Das buttons Feld
	 * wird instanziert. Die Methode buttonsGenerieren wird aufgerufen, um die
	 * Buttons zu dem Fenster hinzuzufügen. Der Spieler wird auf den Anfangswert
	 * gesetzt.
	 */

	public Spiel() {
		super("TicTacToe");
		setSize(710, 900);
		setVisible(true);
		setAlwaysOnTop(false);
		setResizable(false);
		setIgnoreRepaint(true);
		setLocationRelativeTo(null);
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setForeground(Color.WHITE);
		getContentPane().setBackground(Color.WHITE);
		spiel = this;
		this.buttons = new SpielButton[9];
		this.buttonsGenerieren();
		this.setSpieler('O');
	}

	/*
	 * Main Methode Ruft nur den Konstruktor von der Klasse Spiel auf.
	 */
	public static void main(String[] args) {
		new Spiel();
	}

	/*
	 * Methode zum Zeichnen der Spielfläche. Zuerst wird die Füllfarbe der
	 * Rechtecke bestimmt. Danach werden die Rechtecke durch die Methode
	 * fillRect gezeichnet. Der erste Übergabewert ist ein Integer und stellt
	 * die x-Koordinate dar. Der zweite Übergabewert ist ein Integer und stellt
	 * die y-Koordinate dar. Der dritte Übergabewert ist ein Integer und stellt
	 * die Breite dar. Der vierte Übergabewert ist ein Integer und stellt die
	 * Höhe dar.
	 */

	public void paint(Graphics g) {

		g.setColor(Color.BLACK);
		g.fillRect(233, 131, 25, 650);
		g.fillRect(458, 131, 25, 650);
		g.fillRect(33, 331, 650, 25);
		g.fillRect(33, 556, 650, 25);
	}

	/*
	 * Gibt den "Spieler" zurück. Hierbei wird zur einfachen Verwaltung der
	 * Buttons ein char verwendet
	 */

	public char getSpieler() {
		return this.spieler;
	}

	/*
	 * Gibt den Array zurück, in dem alle zur Zeit aktiven Buttons gespeichert
	 * sind.
	 */

	public SpielButton[] getButtons() {
		return this.buttons;
	}

	/*
	 * Setzt den Wert für die Spielervariable neu, sodass der Spieler nach jedem
	 * Zug korrekt gesetzt wird. Wenn ein Spieler gewinnt, wird der Zustand auf
	 * G gesetzt, um Fehler zu vermeiden.
	 */

	public void spielerWechseln() {
		if (getSpieler() == 'X') {
			setSpieler('O');
		} else if (getSpieler() == 'O') {
			setSpieler('X');
			buttons[kiZieht(buttons)].doClick();
		} else {
			setSpieler('G');
		}
	}

	/*
	 * Diese Abfrage überprüft nach jedem Zug, ob das Spiel von einem Spieler
	 * gewonnen wurde oder nicht. Sollte das Spiel gewonnen werden, wird die
	 * Gewinn-Methode für den korrekten Spieler ausgeführt. Um zu überprüfen, ob
	 * 3 gleiche Zeichen in einer Reihe sind, wird jede Möglichkeit durch eine
	 * if-Abfrage überprüft.
	 */

	public boolean gewonnenAbfrage() {

		boolean unentschieden = true;

		char[] c = new char[9];

		for (int i = 0; i < 9; i++) {
			c[i] = buttons[i].getText().toCharArray()[0];
			if (c[i] == ' ') {
				unentschieden = false;
			}
		}
		if (c[0] == spieler && c[1] == spieler && c[2] == spieler
				|| c[0] == spieler && c[3] == spieler && c[6] == spieler
				|| c[0] == spieler && c[4] == spieler && c[8] == spieler
				|| c[1] == spieler && c[4] == spieler && c[7] == spieler
				|| c[2] == spieler && c[5] == spieler && c[8] == spieler
				|| c[2] == spieler && c[4] == spieler && c[6] == spieler
				|| c[3] == spieler && c[4] == spieler && c[5] == spieler
				|| c[6] == spieler && c[7] == spieler && c[8] == spieler) {

			if (spieler == 'O') {
				gewinnAnimation(1);
			} else {
				gewinnAnimation(2);
			}
			return true;
		} else if (unentschieden) {
			gewinnAnimation(5);
			return true;
		}
		return false;
	}

	/*
	 * Setzt die Spieler-Variable zu einem char, der als Übergabeparameter
	 * übergeben wird
	 */

	private char setSpieler(char neu) {

		return this.spieler = neu;

	}

	/*
	 * Hier werden alle Buttons erstellt: Sie werden durch 2 for-Schleifen an
	 * die richtigen Positionen auf dem ContentPane des JFrames gesetzt und in
	 * den Array eingetragen.
	 */

	private void buttonsGenerieren() {
		int counter = 0;
		for (int i = 25; i < 700; i += 225) {
			for (int j = 100; j < 775; j += 225) {
				SpielButton temp = new SpielButton(i + 5, j + 6);
				add(temp);
				this.buttons[counter] = temp;
				counter++;
			}
		}
	}

	/*
	 * Behandelt den Fall, dass ein bestimmter Spieler, der als
	 * Übergabeparameter übergeben wird, gewinnt. Hierbei werden alle noch
	 * aktiven Buttons deaktiviert, sodass kein Button mehr angeklickt werden
	 * kann. Dies ist allerdings nicht zwingend notwendig. Danach wird ein
	 * MessageDialog durch ein JOptionPane mit einerm Informationstext
	 * angezeigt, das alte Spiel geschlossen und ein neues erstellt. Sollte 5
	 * übergeben werden, tritt ein Unentschieden ein.
	 */

	private void gewinnAnimation(int i) {
		setSpieler('G');
		for (SpielButton btm : buttons) {
			if (btm.getText().equals(" "))
				btm.setEnabled(false);
		}
		if (i != 5) {
			JOptionPane.showMessageDialog(null, "Spieler " + i + " hat gewonnen!");

		} else {
			JOptionPane.showMessageDialog(null, "Unentschieden!");
		}
		dispose();
		new Spiel();
	}

	public int kiZieht(SpielButton[] spiel) {
		for (int i = 1; i <= 2; i++) {
			for (int j = 0; j < 9; j++) {
				if (spiel[j].getText().equals(((i == 1) ? "X" : "O"))) {
					/*
					 * Zuerst wird nach waagerechten Zweierpaaren gesucht.
					 */
					if ((j % 3) == 0) {
						if (spiel[j + 2].getText().equals(((i == 1) ? "X" : "O"))) {
							if (spiel[j + 1].getText().equals(" ")) {

								return j + 1;

							}
						} else if (spiel[j + 1].getText().equals(((i == 1) ? "X" : "O"))) {
							if (spiel[j + 2].getText().equals(" ")) {
								return j + 2;
							}
						}
					} else if (j % 3 == 1) {
						if (spiel[j + 1].getText().equals(((i == 1) ? "X" : "O"))) {
							if (spiel[j - 1].getText().equals(" ")) {
								return j - 1;
							}
						}
					}
					/*
					 * Dann wird nach senkrechten Zeichenpaaren gesucht.
					 */
					if (j < 3) {
						if (spiel[j + 3].getText().equals(((i == 1) ? "X" : "O"))) {

							if (spiel[j + 6].getText().equals(" ")) {
								return j + 6;
							}
						} else if (spiel[j + 6].getText().equals(((i == 1) ? "X" : "O"))) {
							if (spiel[j + 3].getText().equals(" ")) {
								return j + 3;
							}
						}
					} else if (j < 6 && j > 2) {
						if (spiel[j + 3].getText().equals(((i == 1) ? "X" : "O"))) {
							if (spiel[j - 3].getText().equals(" ")) {
								return j - 3;
							}
						}
					}
				}

				/*
				 * Als letztes wird nach diagonalen Zeichenpaaren gesucht.
				 */

				if (spiel[4].getText().equals(((i == 1) ? "X" : "O"))) {
					for (int k = 0; k < 9; k++) {
						if (spiel[k].getText().equals(((i == 1) ? "X" : "O")) && k != 4) {
							if (spiel[8 - k].getText().equals(" ")) {
								return (8 - k);
							}
						}
					}
				} else {
					/*
					 * Falls das mittlere Feld leer ist, wird es gefällt.
					 */

					if (spiel[4].getText().equals(" ")) {
						return 4;
					}
				}

			}
		}
		/*
		 * Als nächstes wird geschaut, ob einer von vier Sonderzügen benötigt
		 * wird.
		 */

		for (int i = 0; i < 4; i++) {
			if (spiel[4].getText().equals("O")) {
				// Sonderzug 1
				if (spiel[(i == 3) ? 8 : (i * (i + 1))].getText().equals("O")
						&& spiel[8 - ((i == 3) ? 8 : (i * (i + 1)))].getText().equals("X")) {
					if (spiel[2].getText().equals(" ")) {
						return 2;
					}
				}
			} else if (spiel[4].getText().equals("X")) {

				// Sonderzug 2
				if ((spiel[0].getText().equals("O") && spiel[8].getText().equals("O"))
						|| (spiel[2].getText().equals("O") && spiel[6].getText().equals("O"))) {
					if (spiel[1].getText().equals(" ")) {
						return 1;
					}
				}

				// Sonderzug 3

				if (spiel[(i * 2) + 1].getText().equals("O")) {
					if (i == 0) {
						if (spiel[3].getText().equals("O")) {
							if (spiel[0].getText().equals(" ")) {
								return 0;
							}
						} else if (spiel[5].getText().equals("O")) {
							if (spiel[2].getText().equals(" ")) {
								return 2;
							}
						}
					} else if (i == 3) {
						if (spiel[3].getText().equals("O")) {
							if (spiel[6].getText().equals(" ")) {
								return 6;
							}
						} else if (spiel[5].getText().equals("O")) {
							if (spiel[8].getText().equals(" ")) {
								return 8;
							}
						}
					}
				}

				// Sonderzug 4
				if (spiel[(i * 2) + 1].getText().equals("O")) {
					if (i == 0) {
						if (spiel[6].getText().equals("O")) {

							if (spiel[0].getText().equals(" ")) {
								return 0;
							}
						} else if (spiel[8].getText().equals("O")) {
							if (spiel[2].getText().equals(" ")) {
								return 2;
							}
						}
					} else if (i == 1) {
						if (spiel[2].getText().equals("O")) {
							if (spiel[0].getText().equals(" ")) {
								return 0;
							}
						} else if (spiel[8].getText().equals("O")) {
							if (spiel[6].getText().equals(" ")) {
								return 6;
							}
						}
					} else if (i == 2) {
						if (spiel[0].getText().equals("O")) {
							if (spiel[2].getText().equals(" ")) {
								return 2;
							}
						} else if (spiel[6].getText().equals("O")) {
							if (spiel[8].getText().equals(" ")) {
								return 8;
							}
						}

					} else if (i == 3) {
						if (spiel[0].getText().equals("O")) {
							if (spiel[6].getText().equals(" ")) {
								return 6;
							}
						} else if (spiel[2].getText().equals("O")) {
							if (spiel[8].getText().equals(" ")) {
								return 8;
							}
						}
					}
				}
			}
		}

		/*
		 * Falls es egal ist welcher Zug gemacht wird, wird das erste freie Feld
		 * gefüllt.
		 */
		for (int i = 0; i < 9; i++) {

			if (spiel[i].getText().equals(" ")) {
				return i;
			}
		}
		// Wird niemals eintreten.
		return 10;
	}
}