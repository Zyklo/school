package schule;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

/*
* SpielButton 
* Alle auf dem ContentPane verwendeten Buttons sind Objekte
* dieser Klasse. Durch die Vererbung, erben diese Buttons alle
* Eigenschaften/Attribute/Methoden der Überklasse JButton und werden
* mithilfe der zur Verfügung stehenden Methoden spezifiziert. Der
* ActionListener der einzelnen Button wird ebenfalls als anonymes
* Interface spezifiziert.
* 
* Klasse Unterklasse von JButton
* 
* Autor: Niclas Schümann
*/

@SuppressWarnings("serial")
public class SpielButton extends JButton {

	/*
	 * Instanz des Objektes
	 */

	private SpielButton button;

	/*
	 * Die Übergabewerte int x und int y stellen die Koordinaten auf dem
	 * ContentPane des JFrames dar. Es werden einige Methoden zur Verbesserung
	 * der visuellen Darstellung des Buttons verwendet. Zuletzt wird der
	 * ActionListener definiert: Hier wird überprüft, ob der Button schon von
	 * einem Spieler benutzt wurde oder nicht. Ist dieser schon benutzt worden
	 * oder ist das Spiel bereits gewonnen, wird nichts weiter ausgeführt.
	 * Sollte der Button noch nicht benutzt worden sein, wird er dem richtigen
	 * Spieler zugewiesen. Danach wird abgefragt, ob der Spieler, der am Zug ist,
	 * gewonnen hat. Zuletzt wird der Spieler gewechselt.
	 */

	public SpielButton(int x, int y) {
		super();

		button = this;

		setForeground(Color.BLACK);
		setBackground(Color.WHITE);
		setFocusable(false);
		setFocusPainted(false);
		setRolloverEnabled(false);
		setContentAreaFilled(false);
		setBorderPainted(false);
		setLayout(null);
		setCursor(new Cursor(Cursor.HAND_CURSOR));
		setFont(new Font("Arial", Font.BOLD, 150));
		setBounds(x, y, 200, 200);
		setText(" ");
		setVisible(true);

		addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (getText().equals("X") || getText().equals("O") || Spiel.spiel.getSpieler() == 'G') {
					return;
				} else {
					getButton().setText("" + Spiel.spiel.getSpieler());
					getButton().setEnabled(false);
					if (Spiel.spiel.gewonnenAbfrage()) {
						return;
					}
					Spiel.spiel.spielerWechseln();
				}
			}
		});
	}

	/*
	 * Gibt die Instanz des Objektes zurück, damit diese im ActionListener
	 * verwendet werden kann.
	 */

	private SpielButton getButton() {
		return this.button;
	}
}